/**
 ******************************************************************************
 * @file    tx_initialize_low_level.c
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   tx_initialize_low_level c file
 */

/* Private includes ----------------------------------------------------------*/
#include <stdint.h>
#include "tx_api.h"
#include "sysctrl.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#if defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050) /* ARM Compiler V6 */

    extern unsigned int Image$$RW_IRAM1$$ZI$$Limit;
    #define _TX_UNUSED_MEMORY  ((void *)&Image$$RW_IRAM1$$ZI$$Limit)

    extern unsigned int __Vectors;
    #define _TX_INITIAL_SP __Vectors

#elif defined(__CC_ARM) || defined(__CLANG_ARM)  /* ARM Compiler V5 */

    extern unsigned int Image$$RW_IRAM1$$ZI$$Limit;
    #define _TX_UNUSED_MEMORY  ((void *)&Image$$RW_IRAM1$$ZI$$Limit)

    extern unsigned int __Vectors;
    #define _TX_INITIAL_SP __Vectors

#elif __ICCARM__

    #pragma section="HEAP"
    #define _TX_UNUSED_MEMORY  (__segment_end("HEAP"))

    extern unsigned int __vector_table;
    #define _TX_INITIAL_SP __vector_table

#elif defined ( __GNUC__ )

    extern unsigned int __ram_segment_used_end__;
    #define _TX_UNUSED_MEMORY  ((void *)&__ram_segment_used_end__)

    extern unsigned int g_pfnVectors;
    #define _TX_INITIAL_SP g_pfnVectors

#else /* __GNUC__ */
    #error "not support complier"
#endif

#define HEAP_END    STM32_SRAM_END

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern VOID *_tx_initialize_unused_memory;
extern VOID *_tx_thread_system_stack_ptr;

/* Private function ----------------------------------------------------------*/
VOID _tx_thread_context_save(VOID);
VOID _tx_thread_context_restore(VOID);
VOID _tx_timer_interrupt(VOID);
void Error_Handler(void);


/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
    _tx_execution_isr_enter()                // @ Call the ISR enter function
#endif
    _tx_timer_interrupt();
#ifdef TX_ENABLE_EXECUTION_CHANGE_NOTIFY
    _tx_execution_isr_exit()             //     @ Call the ISR exit function
#endif
}

/*!< STM32G0xx uses 2 Bits for the Priority Levels */
void _tx_pendsv_init(uint32_t PendSV_Priority)
{
    if (PendSV_Priority < (1UL << __NVIC_PRIO_BITS))
    {
        NVIC_SetPriority(PendSV_IRQn, PendSV_Priority);
        NVIC_EnableIRQ(PendSV_IRQn);
    }
    else
    {
        /* NVIC Priority Config Error*/
        Error_Handler();
    }
}

void _tx_systick_init(uint32_t uwTickFreq, uint32_t SysTick_Priority)
{
    if (SysTick_Config(SystemCoreClock / uwTickFreq) == 0U)
    {
        /* Configure the SysTick IRQ priority */
        if (SysTick_Priority < (1UL << __NVIC_PRIO_BITS))
        {
            NVIC_SetPriority(SysTick_IRQn, SysTick_Priority);
            NVIC_EnableIRQ(SysTick_IRQn);
        }
        else
        {
            /* NVIC Priority Config Error*/
            Error_Handler();
        }
    }
}

void _tx_initialize_low_level(void)
{
    TX_INTERRUPT_SAVE_AREA
    TX_DISABLE

    _tx_initialize_unused_memory = (uint8_t *)(_TX_UNUSED_MEMORY) + 4;
    _tx_thread_system_stack_ptr = (uint8_t *)(_TX_INITIAL_SP);

    _tx_systick_init(TX_TIMER_TICKS_PER_SECOND, 1);
    _tx_pendsv_init(3);

    TX_RESTORE
}

/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
