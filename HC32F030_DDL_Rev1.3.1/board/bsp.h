/**
 ******************************************************************************
 * @file    bsp.h
 * @author  Iron
 * @date    2020-01-24
 * @version v1.0
 * @brief   bsp header file
 */

#ifndef __BSP_H
#define __BSP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include <stdint.h>
#include <HC32F030.h>

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
void HAL_Init(void);
void SystemClock_Config(void);
int32_t bsp_init(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
