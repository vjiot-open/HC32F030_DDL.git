/**
 ******************************************************************************
 * @file    bsp_uart0.c
 * @author  Iron
 * @date    2020-01-24
 * @version v1.0
 * @brief   bsp_uart0 c file
 */

/* Private includes ----------------------------------------------------------*/
#include "bsp.h"
#include "bsp_uart0.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define UART_PORT               UARTCH0
//#define UART_PORT_TX_GPIO_PORT  GpioPortA // TX: UART0:PA9  / UART1:PA2
//#define UART_PORT_TX_GPIO_PIN   GpioPin9
//#define UART_PORT_RX_GPIO_PORT  GpioPortA // RX: UART0:PA10 / UART1:PA3
//#define UART_PORT_RX_GPIO_PIN   GpioPin10
//#define GPIOAF                  GpioAf1
//#define SYSCTRL_PERIPHERAL_UART SysctrlPeripheralUart0

#define UART_PORT               UARTCH1
#define UART_PORT_TX_GPIO_PORT  GpioPortA // TX: UART0:PA9  / UART1:PA2
#define UART_PORT_TX_GPIO_PIN   GpioPin2
#define UART_PORT_RX_GPIO_PORT  GpioPortA // RX: UART0:PA10 / UART1:PA3
#define UART_PORT_RX_GPIO_PIN   GpioPin3
#define GPIOAF                  GpioAf1
#define SYSCTRL_PERIPHERAL_UART SysctrlPeripheralUart1

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

static void bsp_uart_port_init(void)
{
    stc_gpio_config_t stcGpioCfg;

    DDL_ZERO_STRUCT(stcGpioCfg);

//    stcGpioCfg.enPuPd = GpioPu;
//    stcGpioCfg.enDrv = GpioDrvH;
//    stcGpioCfg.enOD = GpioOdDisable;
////    stcGpioCfg.enCtrlMode = GpioFastIO;
    stcGpioCfg.enDir = GpioDirOut;
    Gpio_Init(UART_PORT_TX_GPIO_PORT, UART_PORT_TX_GPIO_PIN, &stcGpioCfg);
    Gpio_SetAfMode(UART_PORT_TX_GPIO_PORT, UART_PORT_TX_GPIO_PIN, GPIOAF);

    stcGpioCfg.enDir = GpioDirIn;
    Gpio_Init(UART_PORT_RX_GPIO_PORT, UART_PORT_RX_GPIO_PIN, &stcGpioCfg);
    Gpio_SetAfMode(UART_PORT_RX_GPIO_PORT, UART_PORT_RX_GPIO_PIN, GPIOAF);
}

static void TxIntCallback(void)
{

}

static void RxIntCallback(void)
{

}

static void ErrIntCallback(void)
{

}

static void PErrIntCallBack(void)
{

}

void CtsIntCallBack(void)
{

}

int32_t bsp_uart0_send(const uint8_t *buf, int32_t buflen, uint32_t timeout)
{
    int32_t i;

    if (Uart_GetIsr(UART_PORT) & 0x14) // 错误请求
    {
        Uart_ClrIsr(UART_PORT);
    }

    if (Uart_GetStatus(UART_PORT, UartRC))
    {
        Uart_ClrStatus(UART_PORT, UartRC);
        Uart_ReceiveData(UART_PORT);
    }

    for (i = 0; i < buflen; i++)
    {
        Uart_SendData(UART_PORT, buf[i]);
    }

    return i;
}

int32_t bsp_uart0_recv(uint8_t *buf, int32_t buflen, uint32_t timeout)
{
    int32_t i;

    for (i = 0; i < buflen; i++)
    {
        if (Uart_GetStatus(UART_PORT, UartRC) != TRUE)
            break;

        Uart_ClrStatus(UART_PORT, UartRC);
        buf[i] = Uart_ReceiveData(UART_PORT);
    }

    return i;
}

int32_t bsp_uart0_init(void)
{
    stc_uart_config_t stcConfig;
    stc_uart_irq_cb_t stcUartIrqCb;
    stc_uart_multimode_t stcMulti;
    stc_uart_baud_t stcBaud;
    uint16_t u16Scnt;
    en_uart_mmdorck_t enTb8;

    DDL_ZERO_STRUCT(stcConfig);
    DDL_ZERO_STRUCT(stcUartIrqCb);
    DDL_ZERO_STRUCT(stcMulti);
    DDL_ZERO_STRUCT(stcBaud);

    //外设模块时钟使能
    Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE);
    Sysctrl_SetPeripheralGate(SYSCTRL_PERIPHERAL_UART, TRUE);

    // GPIO 端口初始化
    bsp_uart_port_init();

    stcMulti.enMulti_mode = UartNormal;   //正常工作模式
    Uart_SetMultiMode(UART_PORT, &stcMulti);  //多主机单独配置

    // UartDataOrAddr 由软件读写SBUF[8]来决定
    Uart_SetMMDOrCk(UART_PORT, UartDataOrAddr);

    stcUartIrqCb.pfnRxIrqCb   = TxIntCallback;      ///<发送中断服务函数
    stcUartIrqCb.pfnTxIrqCb   = RxIntCallback;      ///<接收中断服务函数
    stcUartIrqCb.pfnRxFEIrqCb = ErrIntCallback;     ///<接收帧错误中断服务函数
    stcUartIrqCb.pfnPEIrqCb   = PErrIntCallBack;    ///<奇偶校验错误中断服务函数
    stcUartIrqCb.pfnCtsIrqCb  = CtsIntCallBack;     ///<CTS信号翻转中断服务函数
    stcConfig.pstcIrqCb       = &stcUartIrqCb;
    stcConfig.bTouchNvic      = FALSE;
    stcConfig.enRunMode       = UartMode3;    // 模式3
    stcConfig.enStopBit       = Uart1bit;     // 1位停止位
    Uart_Init(UART_PORT, &stcConfig);

    //采样分频, 波特率设置
    Uart_SetClkDiv(UART_PORT, Uart8Or16Div);

    stcBaud.u32Pclk   = Sysctrl_GetPClkFreq();
    stcBaud.enRunMode = UartMode3;
    stcBaud.u32Baud   = 9600;
    u16Scnt = Uart_CalScnt(UART_PORT, &stcBaud); //波特率值计算
    Uart_SetBaud(UART_PORT, u16Scnt);

    Uart_ClrStatus(UART_PORT, UartRC);    //清接收请求
    Uart_EnableFunc(UART_PORT, UartRx);   //使能收发

    enTb8 = UartDataOrAddr;//Bit8由软件读写决定
    Uart_SetMMDOrCk(UARTCH1, enTb8);
    Uart_ClrStatus(UARTCH1, UartTC);

    return 0;
}

/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
