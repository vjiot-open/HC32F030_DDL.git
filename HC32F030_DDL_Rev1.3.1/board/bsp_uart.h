/**
 ******************************************************************************
 * @file    bsp_uart0.h
 * @author  Iron
 * @date    2020-01-24
 * @version v1.0
 * @brief   bsp_uart0 header file
 */

#ifndef __BSP_UART0_H
#define __BSP_UART0_H

#ifdef __cplusplus
extern "C" {
#endif

/* Exported includes ---------------------------------------------------------*/
#include <stdint.h>
#include "gpio.h"
#include "uart.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
int32_t bsp_uart0_send(const uint8_t *buf, int32_t buflen, uint32_t timeout);
int32_t bsp_uart0_recv(uint8_t *buf, int32_t buflen, uint32_t timeout);
int32_t bsp_uart0_init(void);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __BSP_UART0_H */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
