/**
 ******************************************************************************
 * @file    bsp.c
 * @author  Iron
 * @date    2020-01-24
 * @version v1.0
 * @brief   bsp c file
 */

/* Private includes ----------------------------------------------------------*/
#include "bsp.h"
#include "bsp_led.h"
#include "bsp_uart0.h"
#include "flash.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

void HAL_Init(void)
{

}

void SystemClock_Config(void)
{
    stc_sysctrl_clk_config_t stcCfg;
    stc_sysctrl_pll_config_t stcPLLCfg;

    ///< 开启FLASH外设时钟
    Sysctrl_SetPeripheralGate(SysctrlPeripheralFlash, TRUE);

    ///< 因将要倍频的PLL作为系统时钟HCLK会达到48MHz：所以此处预先设置FLASH 读等待周期为1 cycle(默认值为0 cycle)
    Flash_WaitCycle(FlashWaitCycle1);

    ///< 时钟初始化前，优先设置要使用的时钟源：此处配置PLL
    Sysctrl_SetRCHTrim(SysctrlRchFreq4MHz);             //PLL使用RCH作为时钟源，因此需要先设置RCH

    stcPLLCfg.enInFreq    = SysctrlPllInFreq4_6MHz;     //RCH 4MHz
    stcPLLCfg.enOutFreq   = SysctrlPllOutFreq36_48MHz;  //PLL 输出48MHz
    stcPLLCfg.enPllClkSrc = SysctrlPllRch;              //输入时钟源选择RCH
    stcPLLCfg.enPllMul    = SysctrlPllMul12;            //4MHz x 12 = 48MHz
    Sysctrl_SetPLLFreq(&stcPLLCfg);

    ///< 选择PLL作为HCLK时钟源;
    stcCfg.enClkSrc  = SysctrlClkPLL;
    ///< HCLK=SYSCLK/2
    stcCfg.enHClkDiv = SysctrlHclkDiv2; // HCLK: 24MHz
    ///< PCLK=HCLK/1
    stcCfg.enPClkDiv = SysctrlPclkDiv1; // PCLK: 24MHz
    ///< 系统时钟初始化
    Sysctrl_ClkInit(&stcCfg);

    //更新Core时钟（HCLK）
    SystemCoreClockUpdate();
}

int32_t bsp_init(void)
{
    /* Boadr Configuration */
    bsp_led_init();
    bsp_uart0_init();

    return 0;
}

/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
