/**
 ******************************************************************************
 * @file    bsp_led.c
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   bsp_led c file
 */

/* Private includes ----------------------------------------------------------*/
#include "bsp.h"
#include "gpio.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define LED_PORT    GpioPortD
//#define LED_PIN     GpioPin5
#define LED_PORT    GpioPortB
#define LED_PIN     GpioPin9

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

int32_t bsp_led_init(void)
{
    stc_gpio_config_t pstcGpioCfg;

    Sysctrl_SetPeripheralGate(SysctrlPeripheralGpio, TRUE); // < 打开GPIO外设时钟门控

    pstcGpioCfg.enDir = GpioDirOut;         // < 端口方向配置->输出
    pstcGpioCfg.enDrv = GpioDrvH;           // < 端口驱动能力配置->高驱动能力
    pstcGpioCfg.enPuPd = GpioNoPuPd;        // < 端口上下拉配置->无上下拉
    pstcGpioCfg.enOD = GpioOdDisable;       // < 端口开漏输出配置->开漏输出关闭
    pstcGpioCfg.enCtrlMode = GpioAHB;       // < 端口输入/输出值寄存器总线控制模式配置->AHB

    Gpio_Init(LED_PORT, LED_PIN, &pstcGpioCfg);  // < GPIO IO PD05初始化（PD05在STK上外接LED）

    return 0;
}

int32_t bsp_led_on(int32_t led_id)
{
    Gpio_SetIO(LED_PORT, LED_PIN);

    return 0;
}

int32_t bsp_led_off(int32_t led_id)
{
    Gpio_ClrIO(LED_PORT, LED_PIN);

    return 0;
}


/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
