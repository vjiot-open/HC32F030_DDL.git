/**
 ******************************************************************************
 * @file    main.c
 * @author  Iron
 * @date    2020-01-01
 * @version v1.0
 * @brief   main c file
 */

/* Private includes ----------------------------------------------------------*/
#include <stdint.h>
#include "tx_api.h"
#include "bsp.h"
#include "bsp_led.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define MCU_SRAM_END_ADDR         0x20001FFF // 8K
//#define DEMO_BYTE_POOL_SIZE     4096
#define DEMO_STACK_SIZE         256
#define DEMO_QUEUE_SIZE         10
#define DEMO_BLOCK_POOL_SIZE    100

TX_BYTE_POOL byte_pool_0;

#define TX_MS_TO_TICKS( ms ) ((( ms ) * TX_TIMER_TICKS_PER_SECOND ) / 1000 )

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Define the ThreadX object control blocks...  */
TX_THREAD thread_main;

#define SAMPLE_THREADX_TEST 1

#if SAMPLE_THREADX_TEST
TX_THREAD thread_0;
TX_THREAD thread_1;
TX_THREAD thread_2;
TX_THREAD thread_3;
TX_THREAD thread_4;
TX_THREAD thread_5;
TX_THREAD thread_6;
TX_THREAD thread_7;

TX_QUEUE                queue_0;
TX_SEMAPHORE            semaphore_0;
TX_MUTEX                mutex_0;
TX_EVENT_FLAGS_GROUP    event_flags_0;
TX_BLOCK_POOL           block_pool_0;
#endif

/* Private function prototypes -----------------------------------------------*/
void thread_main_entry(ULONG thread_input);
void thread_0_entry(ULONG thread_input);
void thread_1_entry(ULONG thread_input);
void thread_2_entry(ULONG thread_input);
void thread_3_and_4_entry(ULONG thread_input);
void thread_5_entry(ULONG thread_input);
void thread_6_and_7_entry(ULONG thread_input);

/* Define the test threads.  */
void thread_main_entry(ULONG thread_input)
{
    /* Board Init */
    bsp_init();

    /* This thread simply sits in while-forever-sleep loop.  */
    while (1)
    {
        bsp_led_on(0);
        tx_thread_sleep(TX_MS_TO_TICKS(100));

        bsp_led_off(0);
        tx_thread_sleep(TX_MS_TO_TICKS(100));
    }
}

void tx_application_define(void *first_unused_memory)
{
    CHAR *pointer = TX_NULL;

#ifdef TX_ENABLE_EVENT_TRACE
    tx_trace_enable(trace_buffer, sizeof(trace_buffer), 32);
#endif

    /* Create a byte memory pool from which to allocate the thread stacks.  */
    tx_byte_pool_create(&byte_pool_0, "byte pool 0", first_unused_memory, (MCU_SRAM_END_ADDR - (uint32_t)first_unused_memory)); // DEMO_BYTE_POOL_SIZE

#ifdef TX_ENABLE_EVENT_TRACE
    tx_trace_enable(trace_buffer, sizeof(trace_buffer), 32);
#endif

    /* Put system definition stuff in here, e.g. thread creates and other assorted
       create information.  */

    /* Allocate the stack for thread 0.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);

    /* Create the main thread.  */
    tx_thread_create(&thread_main, "thread main", thread_main_entry, 0,
                     pointer, DEMO_STACK_SIZE, 1, 1, TX_NO_TIME_SLICE,
                     TX_AUTO_START);

#if SAMPLE_THREADX_TEST
    /* Allocate the stack for thread 0.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);

    /* Create the main thread.  */
    tx_thread_create(&thread_0, "thread 0", thread_0_entry, 0,
                     pointer, DEMO_STACK_SIZE,
                     1, 1, TX_NO_TIME_SLICE, TX_AUTO_START);


    /* Allocate the stack for thread 1.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);

    /* Create threads 1 and 2. These threads pass information through a ThreadX
       message queue.  It is also interesting to note that these threads have a time
       slice.  */
    tx_thread_create(&thread_1, "thread 1", thread_1_entry, 1,
                     pointer, DEMO_STACK_SIZE,
                     16, 16, 4, TX_AUTO_START);

    /* Allocate the stack for thread 2.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);

    tx_thread_create(&thread_2, "thread 2", thread_2_entry, 2,
                     pointer, DEMO_STACK_SIZE,
                     16, 16, 4, TX_AUTO_START);

//    /* Allocate the stack for thread 3.  */
//    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);
//
//    /* Create threads 3 and 4.  These threads compete for a ThreadX counting semaphore.
//       An interesting thing here is that both threads share the same instruction area.  */
//    tx_thread_create(&thread_3, "thread 3", thread_3_and_4_entry, 3,
//                     pointer, DEMO_STACK_SIZE,
//                     8, 8, TX_NO_TIME_SLICE, TX_AUTO_START);
//
//    /* Allocate the stack for thread 4.  */
//    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);
//
//    tx_thread_create(&thread_4, "thread 4", thread_3_and_4_entry, 4,
//                     pointer, DEMO_STACK_SIZE,
//                     8, 8, TX_NO_TIME_SLICE, TX_AUTO_START);
//
//    /* Allocate the stack for thread 5.  */
//    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);
//
//    /* Create thread 5.  This thread simply pends on an event flag which will be set
//       by thread_0.  */
//    tx_thread_create(&thread_5, "thread 5", thread_5_entry, 5,
//                     pointer, DEMO_STACK_SIZE,
//                     4, 4, TX_NO_TIME_SLICE, TX_AUTO_START);
//
//    /* Allocate the stack for thread 6.  */
//    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);
//
//    /* Create threads 6 and 7.  These threads compete for a ThreadX mutex.  */
//    tx_thread_create(&thread_6, "thread 6", thread_6_and_7_entry, 6,
//                     pointer, DEMO_STACK_SIZE,
//                     8, 8, TX_NO_TIME_SLICE, TX_AUTO_START);
//
//    /* Allocate the stack for thread 7.  */
//    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_STACK_SIZE, TX_NO_WAIT);
//
//    tx_thread_create(&thread_7, "thread 7", thread_6_and_7_entry, 7,
//                     pointer, DEMO_STACK_SIZE,
//                     8, 8, TX_NO_TIME_SLICE, TX_AUTO_START);

    /* Allocate the message queue.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_QUEUE_SIZE * sizeof(ULONG), TX_NO_WAIT);

    /* Create the message queue shared by threads 1 and 2.  */
    tx_queue_create(&queue_0, "queue 0", TX_1_ULONG, pointer, DEMO_QUEUE_SIZE * sizeof(ULONG));

    /* Create the semaphore used by threads 3 and 4.  */
    tx_semaphore_create(&semaphore_0, "semaphore 0", 1);

    /* Create the event flags group used by threads 1 and 5.  */
    tx_event_flags_create(&event_flags_0, "event flags 0");

    /* Create the mutex used by thread 6 and 7 without priority inheritance.  */
    tx_mutex_create(&mutex_0, "mutex 0", TX_NO_INHERIT);

    /* Allocate the memory for a small block pool.  */
    tx_byte_allocate(&byte_pool_0, (VOID **) &pointer, DEMO_BLOCK_POOL_SIZE, TX_NO_WAIT);

    /* Create a block memory pool to allocate a message buffer from.  */
    tx_block_pool_create(&block_pool_0, "block pool 0", sizeof(ULONG), pointer, DEMO_BLOCK_POOL_SIZE);

    /* Allocate a block and release the block memory.  */
    tx_block_allocate(&block_pool_0, (VOID **) &pointer, TX_NO_WAIT);

    /* Release the block back to the pool.  */
    tx_block_release(pointer);
#endif
}

int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
//    HAL_Init();

    /* Configure the system clock */
//    SystemClock_Config();

    /* Enter the ThreadX kernel.  */
    tx_kernel_enter();

    return 0;
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
    }
    /* USER CODE END Error_Handler_Debug */
}

/**
 * @}
 */

/******************* (C)COPYRIGHT 2020 ***** END OF FILE *********************/
