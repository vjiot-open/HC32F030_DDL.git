================================================================================
                                样例使用说明
================================================================================
版本历史 
日期        版本    负责人         IAR     	MDK   	描述
2021-01-23  1.0     xwiron         8.50.6  	5.30  	First Version

================================================================================
功能描述
================================================================================
本样例主要展示GPIO输出功能。
说明：
本样例使用PD05（LED）演示不断输出高低电平，来演示输出功能；

================================================================================
测试环境
================================================================================
测试用板:
---------------------
HC32F030_STK

辅助工具:
---------------------
万用表

辅助软件:
---------------------

================================================================================
使用步骤
================================================================================
1）打开工程并重新编译；
2）启动IDE的下载和调试功能；
3）将程序下载至Demo板；
4）重新上电;
5）GPIO每一秒输出翻转一次，可观测到LED 每隔1s点亮一次；


================================================================================
注意
================================================================================
其它输出方式、驱动能力、上下拉等，可根据实际应用需要来进行配置。

================================================================================
